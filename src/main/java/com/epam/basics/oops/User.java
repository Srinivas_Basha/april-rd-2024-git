package com.epam.basics.oops;

public class User {

    private int id;
    private String name;
    private int age;

    void setId(int id){
        // We can do additional validation inside setter if value is invalid, we will ignore.
        if(id > 0 ){
            this.id = id;
        }
    }

    int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return name + ", "+ id + ", "+ age;
    }
}
