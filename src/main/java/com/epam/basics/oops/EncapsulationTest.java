package com.epam.basics.oops;

public class EncapsulationTest {
    public static void main(String[] args) {
        User user = new User();

        // In the 2)Linking phase , during 2.2)preparation, default values are assigned
        // 2.3) symbolic reference is replaced with original address.
//        System.out.println(user.id + ", "+ user.name+ ", "+ user.age);

        // During 3) Initialization phase, default values replaced with actual values.
//        user.id = -19;
//        user.name = "";
//        user.age = -25;
//        System.out.println(user.id + ", "+ user.name+ ", "+ user.age);

        user.setId(19);
        user.setAge(25);
        user.setName("ABC");
        //Not required if toString() is overridden in User class
//        System.out.println(user.getId() + ", "+ user.getAge()+ ", "+ user.getName());
        System.out.println(user);

    }
}
