package com.epam.basics.classandobject;

public class CustomerTest {
    public static void main(String[] args) {
        // if declared as static, we can call like below.
//        System.out.println(Customer.customerName);
//        System.out.println(Customer.customerDepartment);


        // if not declared as static, we can call do like below. Default values will be printed if no values are given
        Customer customer = new Customer();
        System.out.println(customer.customerName+ ", "+ customer.customerDepartment);

//        if not declared as static, we can call do like below. Default values will be printed but
//        if we want to initialize values. Do like below
        Customer secondCustomer = new Customer();
        secondCustomer.customerName = "ABC";
        secondCustomer.customerDepartment = "XYZ";
        System.out.println(secondCustomer.customerName+ ", "+ secondCustomer.customerDepartment);

        // We can also give values to constructor
        Customer thirdCustomer = new Customer("ABC", "XYZ");
        System.out.println(thirdCustomer.customerName + ", "+ thirdCustomer.customerDepartment);



    }
}
